package br.com.galgo.testes.suite;

import org.junit.experimental.categories.Categories;
import org.junit.experimental.categories.Categories.IncludeCategory;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import br.com.galgo.ValidaHistoricoEventos;
import br.com.galgo.interfaces.TestePos;

@RunWith(Categories.class)
@IncludeCategory(TestePos.class)
@Suite.SuiteClasses({ ValidaHistoricoEventos.class })
public class SuiteValidaPosicaoAtivosPos {

}
